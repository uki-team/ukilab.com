(function() {
  if (!/mobile/i.test(navigator.userAgent)) {return;}
  var APPS = {
    qr: {
      android: "",
      ios: "https://itunes.apple.com/jp/app/qrkodorida-you-bian-zhai-pei/id1138716433?mt=8"
    },
    ukeuke: {
      android: "",
      ios: "https://itunes.apple.com/jp/app/ukeuke-dong-hua-3miaode-jue/id1143588726?mt=8"
    },
    ec: {
      ios: "https://itunes.apple.com/jp/app/ukiec-si-ge-bi-jiao-bakodoridadeecsaitono/id1147494411?mt=8"
    }
  };
  var isAndroid = /Android/i.test(navigator.userAgent);
  var els = document.querySelectorAll('.app-item');
  for (var i = 0; i < els.length; i++) {
    linkApp(els[i]);
  }

  function linkApp(el) {
    var name = el.getAttribute('data-app');
    var link;
    if (isAndroid) {
      link = APPS[name].android;
    }
    el.querySelector('.app-logo').href = link || APPS[name].ios;
  }
})();
